Score = {}
Score.__index = Score

function Score:new(sprites)
    local score = {}
    setmetatable(score, Score)

    score.sprites = sprites
    score.value = 0
    score.digits = { 0 }
    score.space = 2

    return score
end

function Score:increment()
    self.value = self.value + 1

    local value = 1
    for i = #self.digits, 1, -1 do
        value = (self.digits[i] + value) % 10

        if self.digits[i] == 9 then
            self.digits[i] = 0
            value = 1
        else
            self.digits[i] = value
            value = 0
            break
        end
    end

    if value ~= 0 then
        table.insert(self.digits, 1, value)
    end
end

function Score:clear()
    self.value = 0
    self.digits = { 0 }
end

function Score:draw()
    local spriteWidth = self.sprites[0]:getWidth()
    local width = #self.digits * spriteWidth + (#self.digits - 1) * self.space
    local x = (Constants.screenWidth - width) / 2

    for _, digit in pairs(self.digits) do
        love.graphics.draw(self.sprites[digit], x, self.sprites[digit]:getHeight())
        x = x + spriteWidth + self.space
    end
end
