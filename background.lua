Background = {}
Background.__index = Background

function Background:new(sprite)
    local background = {}
    setmetatable(background, Background)

    background.sprite = sprite
    background.sprite:setWrap("repeat", "repeat")
    background.quad = love.graphics.newQuad(0, 0, Constants.screenWidth, Constants.screenHeight, sprite:getWidth()
        , sprite:getHeight())

    return background
end

function Background:draw()
    love.graphics.draw(self.sprite, self.quad, 0, 0)
end
