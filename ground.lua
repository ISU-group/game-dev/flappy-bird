Ground = {}
Ground.__index = Ground

local spriteWidthHalf

function Ground:new(location, sprite)
    local ground = {}
    setmetatable(ground, Ground)

    ground.location = location
    ground.sprite = sprite
    ground.sprite:setWrap("repeat", "repeat")

    spriteWidthHalf = ground.sprite:getWidth() / 2

    ground.quad = love.graphics.newQuad(
        0, 0, Constants.screenWidth + spriteWidthHalf,
        sprite:getHeight(),
        sprite:getWidth(),
        sprite:getHeight())

    return ground
end

function Ground:update(dt, birdVelocity)
    self.location.x = (self.location.x - birdVelocity.x * dt) % spriteWidthHalf
end

function Ground:draw()
    love.graphics.draw(self.sprite, self.quad, self.location.x - spriteWidthHalf, self.location.y)
end
