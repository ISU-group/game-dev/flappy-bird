require "resources"
require "game"
require "background"
require "constants"
require "ground"
require "vector"
require "bird"
require "pipe"
require "pipe_manager"
require "pipe_pair"
require "score"
require "game_state"

local assetsDir = 'assets'
local game

local function loadResources()
    return Resources:new({
        ["audio"] = {
            ["die"] = table.concat({ assetsDir, "audio", "die.wav" }, "/"),
            ["hit"] = table.concat({ assetsDir, "audio", "hit.wav" }, "/"),
            ["wing"] = table.concat({ assetsDir, "audio", "wing.wav" }, "/"),
            ["point"] = table.concat({ assetsDir, "audio", "point.wav" }, "/"),
            ["swoosh"] = table.concat({ assetsDir, "audio", "swoosh.wav" }, "/"),
        },
        ["sprites"] = {
            ["background"] = table.concat({ assetsDir, "sprites", "background-day.png" }, "/"),
            ["ground"] = table.concat({ assetsDir, "sprites", "base.png" }, "/"),

            ["bird"] = {
                ["up"] = table.concat({ assetsDir, "sprites", "yellowbird-upflap.png" }, "/"),
                ["mid"] = table.concat({ assetsDir, "sprites", "yellowbird-midflap.png" }, "/"),
                ["down"] = table.concat({ assetsDir, "sprites", "yellowbird-downflap.png" }, "/"),
            },

            ["pipe"] = table.concat({ assetsDir, "sprites", "pipe-green.png" }, "/"),

            ["score"] = {
                [0] = table.concat({ assetsDir, "sprites", "0.png" }, "/"),
                [1] = table.concat({ assetsDir, "sprites", "1.png" }, "/"),
                [2] = table.concat({ assetsDir, "sprites", "2.png" }, "/"),
                [3] = table.concat({ assetsDir, "sprites", "3.png" }, "/"),
                [4] = table.concat({ assetsDir, "sprites", "4.png" }, "/"),
                [5] = table.concat({ assetsDir, "sprites", "5.png" }, "/"),
                [6] = table.concat({ assetsDir, "sprites", "6.png" }, "/"),
                [7] = table.concat({ assetsDir, "sprites", "7.png" }, "/"),
                [8] = table.concat({ assetsDir, "sprites", "8.png" }, "/"),
                [9] = table.concat({ assetsDir, "sprites", "9.png" }, "/"),
            },

            ["messages"] = {
                ["greeting"] = table.concat({ assetsDir, "sprites", "message.png" }, "/"),
                ["gameover"] = table.concat({ assetsDir, "sprites", "gameover.png" }, "/")
            }
        }
    })
end

function love.load()
    game = Game:new(loadResources())
end

function love.update(dt)
    game:update(dt)
end

function love.draw()
    game:draw()
end

function love.keypressed(key, scancode, isrepeat)
    if key == "space" then
        if game.state:isGameOver() then
            game:restart()
            return
        elseif game.state:isGreeting() then
            game.resources.audio.swoosh:play()
        end

        game.state:play()
        game.bird:flap()

        game.resources.audio.wing:play()
    end
end
