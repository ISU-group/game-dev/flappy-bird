Constants = {
    screenWidth = love.graphics.getWidth(),
    screenHeight = love.graphics.getHeight(),

    birdSpriteDelta = 0.125,
    birdFlapSpeedY = -4.89,
    birdMaxSpeedY = 12,

    gravity = 16,

    pipesDeltaTime = 1.2,
    pipesGapY = 100,
    pipeOffsetX = 100
}
