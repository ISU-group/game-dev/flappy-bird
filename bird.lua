Bird = {}
Bird.__index = Bird

function Bird:new(location, sprites)
    local bird = {}
    setmetatable(bird, Bird)

    bird.sprites = sprites
    bird._spriteDeltaTime = 1
    bird._spriteIdx = 1

    bird._fallTime = 0
    bird.isFalling = false

    bird.isActive = false

    bird.location = location
    bird.velocity = Vector:new(180, 0)
    bird.angle = 0

    return bird
end

function Bird:getWidth()
    return self.sprites[self._spriteIdx]:getWidth()
end

function Bird:getHeight()
    return self.sprites[self._spriteIdx]:getHeight()
end

function Bird:nextSprite(dt)
    self._spriteDeltaTime = self._spriteDeltaTime + dt

    if self._spriteDeltaTime > Constants.birdSpriteDelta then
        self._spriteIdx = (self._spriteIdx + 1) % #self.sprites + 1
        self._spriteDeltaTime = 0
    end
end

function Bird:update(dt)
    if not self.isActive then
        return
    end

    self:nextSprite(dt)

    self.velocity.y = math.min(self.velocity.y + Constants.gravity * dt, Constants.birdMaxSpeedY)
    self.location.y = self.location.y + self.velocity.y

    self._fallTime = self._fallTime + dt
    self.isFalling = self._fallTime > 1

    self.angle = self.angle + dt * (self.isFalling and 4 or 1.5)
    self.angle = math.min(self.angle, math.rad(90))
end

function Bird:draw()
    local idx = self.isFalling and 1 or self._spriteIdx
    love.graphics.draw(self.sprites[idx], self.location.x, self.location.y, self.angle)
end

function Bird:flap()
    self.velocity.y = Constants.birdFlapSpeedY
    self.angle = -math.rad(45)

    self._fallTime = 0
    self.isActive = true
end

function Bird:hasGroundCollision(ground)
    return self.location.y + self:getHeight() - 4 > ground.location.y
end

function Bird:handleGroundCollision(ground)
    if self.isActive then
        self.velocity:mul(0)
        self.location.y = ground.location.y - 4 - self:getHeight()

        self.isActive = false
    end
end

function Bird:hasPipeCollision(pipe)
    local offset = 6
    local newLocation = Vector:new(self.location.x + offset / 2, self.location.y + offset / 2)

    return (newLocation.x + self:getWidth() - offset >= pipe.location.x
        and newLocation.x <= pipe.location.x + pipe:getWidth()
        and newLocation.y + self:getHeight() - offset >= pipe.location.y
        and newLocation.y <= pipe.location.y + pipe:getHeight())
        or (newLocation.x + self:getWidth() - offset >= pipe.location.x
            and newLocation.y < 0)
end

function Bird:handlePipeCollision(pipe)
    self.velocity.x = 0
end
