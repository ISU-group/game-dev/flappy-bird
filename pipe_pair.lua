PipePair = {}
PipePair.__index = PipePair

function PipePair:new(sprite, ymin, ymax)
    local pair = {}
    setmetatable(pair, PipePair)

    local y = math.random(ymin + Constants.pipesGapY, ymax)
    local x = Constants.screenWidth + Constants.pipeOffsetX
    local upperPipeLocation = Vector:new(x, y - sprite:getHeight())
    local lowerPipeLocation = Vector:new(x, y + Constants.pipesGapY)

    pair.upper = Pipe:new(sprite, upperPipeLocation)
    pair.lower = Pipe:new(sprite, lowerPipeLocation)

    pair.scored = false

    return pair
end

function PipePair:getWidth()
    return self.upper.sprite:getWidth()
end

function PipePair:getHeight()
    return self.upper.sprite:getHeight()
end

function PipePair:isGone()
    return self.upper:isGone()
end

function PipePair:rightX()
    return self.upper.location.x + self:getWidth()
end

function PipePair:update(dt, birdVelocity)
    self.upper:update(dt, birdVelocity)
    self.lower:update(dt, birdVelocity)
end

function PipePair:draw()
    love.graphics.draw(self.upper.sprite, self.upper.location.x, self.upper.location.y, math.pi, 1, 1, self:getWidth(),
        self:getHeight())
    love.graphics.draw(self.lower.sprite, self.lower.location.x, self.lower.location.y)
end
