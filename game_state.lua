GameState = {}
GameState.__index = GameState

local states = {
    gameOver = 0,
    greeting = 1,
    playing = 2,
}

function GameState:new(sprites)
    local state = {}
    setmetatable(state, GameState)

    state.state = states.greeting
    state.score = Score:new(sprites)

    return state
end

function GameState:isGameOver()
    return self.state == states.gameOver
end

function GameState:isPlaying()
    return self.state == states.playing
end

function GameState:isGreeting()
    return self.state == states.greeting
end

function GameState:gameOver()
    self.state = states.gameOver
end

function GameState:play()
    self.state = states.playing
end

function GameState:greeeting()
    self.state = states.greeting
end

function GameState:restart()
    self:greeeting()
    self.score:clear()
end
