PipeManager = {}
PipeManager.__index = PipeManager

function PipeManager:addPair()
    table.insert(self.pipes, PipePair:new(self.sprite, self._ymin, self.sprite:getHeight()))
end

function PipeManager:removeGonePair()
    local leftPipes = {}

    for _, pair in pairs(self.pipes) do
        if not pair:isGone() then
            table.insert(leftPipes, pair)
        end
    end

    self.pipes = leftPipes
end

function PipeManager:new(sprite, ymin, ymax)
    local manager = {}
    setmetatable(manager, PipeManager)

    manager.sprite = sprite
    manager.pipes = {}

    manager._pipesDeltaTime = 0
    manager._ymin = ymin
    manager._ymax = ymax

    return manager
end

function PipeManager:update(dt, birdVelocity)
    self._pipesDeltaTime = self._pipesDeltaTime + dt

    self:removeGonePair()

    if self._pipesDeltaTime > Constants.pipesDeltaTime then
        self._pipesDeltaTime = 0
        self:addPair()
    end

    for _, pair in pairs(self.pipes) do
        pair:update(dt, birdVelocity)
    end
end

function PipeManager:draw()
    for _, pair in pairs(self.pipes) do
        pair:draw()
    end
end

function PipeManager:clear()
    self.pipes = {}
end
