Resources = {}
Resources.__index = Resources

local function loadSounds(paths)
    local sounds = {}
    for name, path in pairs(paths) do
        sounds[name] = love.audio.newSource(path, "static")
    end

    return sounds
end

local function loadSprites(paths)
    local sprites = {}
    for name, path in pairs(paths) do
        if type(path) == "table" then
            sprites[name] = loadSprites(path)
        else
            sprites[name] = love.graphics.newImage(path)
        end
    end

    return sprites
end

function Resources:new(resources_paths)
    local resources = {}
    setmetatable(resources, Resources)

    for resource, paths in pairs(resources_paths) do
        if resource == "audio" then
            resources[resource] = loadSounds(paths)
        elseif resource == "sprites" then
            resources[resource] = loadSprites(paths)
        else
            error("Unknown resource: " .. resource)
        end
    end

    return resources
end
