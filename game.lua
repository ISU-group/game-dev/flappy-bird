Game = {}
Game.__index = Game


function Game:new(resources)
    local game = {}
    setmetatable(game, Game)

    game.resources = resources
    game.background = Background:new(resources.sprites.background)
    game.ground = Ground:new(
        Vector:new(0, game.background.sprite:getHeight()),
        resources.sprites.ground
    )

    local birdLocation = Vector:new(
        (Constants.screenWidth - resources.sprites.bird.mid:getWidth()) / 2,
        (Constants.screenHeight - resources.sprites.bird.mid:getHeight()) / 2
    )
    game.bird = Bird:new(
        birdLocation,
        {
            resources.sprites.bird.mid,
            resources.sprites.bird.up,
            resources.sprites.bird.down
        }
    )

    game.pipeManager = PipeManager:new(resources.sprites.pipe, 0, game.resources.sprites.background:getHeight())

    game.state = GameState:new(resources.sprites.score)

    return game
end

function Game:update(dt)
    if self.bird:hasGroundCollision(self.ground) then
        self.bird:handleGroundCollision(self.ground)
        self.state:gameOver()

        self.resources.audio.die:play()
    end

    if self.state:isPlaying() then
        for _, pair in pairs(self.pipeManager.pipes) do
            if self.bird:hasPipeCollision(pair.upper) then
                self.bird:handlePipeCollision(pair.upper)
                self.state:gameOver()

                self.resources.audio.hit:play()
                break
            elseif self.bird:hasPipeCollision(pair.lower) then
                self.bird:handlePipeCollision(pair.lower)
                self.state:gameOver()

                self.resources.audio.hit:play()
                break
            elseif not pair.scored and pair:rightX() < self.bird.location.x then
                self.state.score:increment()
                pair.scored = true

                self.resources.audio.point:play()
            end
        end

        self.pipeManager:update(dt, self.bird.velocity)
    end

    self.bird:update(dt)
    self.ground:update(dt, self.bird.velocity)
end

function Game:drawGameOver()
    local greetingX = (Constants.screenWidth - self.resources.sprites.messages.gameover:getWidth()) / 2
    local greetingY = (
        self.resources.sprites.background:getHeight() - self.resources.sprites.messages.gameover:getHeight()) / 2

    love.graphics.draw(self.resources.sprites.messages.gameover, greetingX, greetingY)
end

function Game:drawGreeting()
    local greetingX = (Constants.screenWidth - self.resources.sprites.messages.greeting:getWidth()) / 2
    local greetingY = (
        self.resources.sprites.background:getHeight() - self.resources.sprites.messages.greeting:getHeight()) / 2
    love.graphics.draw(self.resources.sprites.messages.greeting, greetingX, greetingY)
end

function Game:draw()
    self.background:draw()

    if self.state:isGreeting() then
        self:drawGreeting()
    else
        self.pipeManager:draw()
        self.state.score:draw()
        self.bird:draw()

        if self.state:isGameOver() then
            self:drawGameOver()
        end
    end

    self.ground:draw()
end

function Game:newBird()
    local birdLocation = Vector:new(
        (Constants.screenWidth - self.resources.sprites.bird.mid:getWidth()) / 2,
        (Constants.screenHeight - self.resources.sprites.bird.mid:getHeight()) / 2
    )
    self.bird = Bird:new(
        birdLocation,
        {
            self.resources.sprites.bird.mid,
            self.resources.sprites.bird.up,
            self.resources.sprites.bird.down
        }
    )
end

function Game:restart()
    self.state:restart()
    self.pipeManager:clear()

    self:newBird()
end
