Pipe = {}
Pipe.__index = Pipe

function Pipe:new(sprite, location, isUpper)
    local pipe = {}
    setmetatable(pipe, Pipe)

    pipe.sprite = sprite
    pipe.location = location

    return pipe
end

function Pipe:getWidth()
    return self.sprite:getWidth()
end

function Pipe:getHeight()
    return self.sprite:getHeight()
end

function Pipe:update(dt, birdVelocity)
    self.location.x = self.location.x - birdVelocity.x * dt
end

function Pipe:isGone()
    return self.location.x + self:getWidth() <= 0
end
